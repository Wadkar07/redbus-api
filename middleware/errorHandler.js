function errorHandler(err, req, res, next){
    return res.status(err.status).send({error:err.message})
}
module.exports = errorHandler;