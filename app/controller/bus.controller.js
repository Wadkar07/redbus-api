const db = require('../models');
const Bus = db.bus;

exports.create = (req, res, next) => {
    const {

        traveler,
        busType,
        typeOfBus,
        departure,
        departureTime,
        duration,
        arrival,
        arrivalTime,
        rating,
        ratingCount,
        price,
        seatsAvailable } = req.body;
    const emptyFields = Object.entries(req.body).reduce((emptyFields, [field, fielsValue]) => {

        if (fielsValue === "") {
            emptyFields.push(field)
        }
        return emptyFields;
    }, []);

    if (emptyFields.length) {
        next({
            status: 400,
            message: `${emptyFields.join(',')} field(s) cannot be empty `,
        })
        
        return;
    }
    else {
        let seats = Array.from(
            {
                length: 21
            },
            (index, value) => {
                return 0
            });
        const bus = new Bus({
            traveler,
            busType,
            typeOfBus,
            departure,
            departureTime,
            duration,
            arrival,
            arrivalTime,
            rating,
            ratingCount,
            price,
            seatsAvailable,
            seatBookedForLowerPlatform: seats,
            seatBookedForUpperPlatform: seats
        })
        bus.save(bus)
            .then((data) => {
                res.send(data);
            })
            .catch(err => {
                next({
                    status: 500,
                    message: "Some error ocuured while creating bus"
                })
                
            })
    }
}

exports.findAll = (req, res,next) => {
    const title = req.query.title;
    let condition = title ? {
        title: {
            $regex: new RegExp(title),
            $options: "i"
        }
    } : {};

    Bus.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Some error occurred while retrieving bus"
            })
            
        })
}

exports.findOne = (req, res,next) => {
    const id = req.params.id;

    Bus.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found Bus with id " + id
                })
            }
            else {
                res.send(data);
            }
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Bus with id=" + id });
        });
};

exports.update = (req, res,next) => {
    if (!req.body) {
        return next({
            status: 400,
            message: "Data to update can not be empty!"
        })
        
    }

    const id = req.params.id;

    Bus.findByIdAndUpdate(id, req.body,
        {
            useFindAndModify: false
        })
        .then(data => {
            if (!data) {
                next({
                    status: 400,
                    message: `Cannot update bus with id=${id}. Maybe bus was not found!`
                })
                
            }
            else {
                
                res.send(
                    {
                        message: "Bus was updated successfully."
                    }
                )
            };
        })
        .catch((err) => {
            next({
                status: 500,
                message: `Error updating bus with id : ${id}`
            })
           
        });
};

exports.delete = (req, res,next) => {
    const id = req.params.id;
    Bus.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 400,
                    message: `Cannot delete Bus with id : ${id}. May be article was not found`
                })
                
            }
            else {
                res.send(
                    {
                        message: "Bus deleted successfully"
                    }
                )
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: `Could not delete bus with id : ${id}`
            })
            
        })
};

exports.deleteAll = (req, res,next) => {
    Bus.deleteMany({})
        .then((data) => {
            res.send(
                {
                    message: `${data.deletedCount} bus were deleted succesfully`
                }
            )
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occured while deleting all buses"
            })
            
        })
};