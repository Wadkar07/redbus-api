const dbConfig = require("../config/config.js");
const busModel = require("./bus.model.js");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.bus = busModel(mongoose);

module.exports = db;