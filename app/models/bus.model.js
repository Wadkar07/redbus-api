module.exports = (mongoose) => {
    let schema = mongoose.Schema(
        {
            traveler: String,
            busType: String,
            typeOfBus: String,
            departure: String,
            departureTime: String,
            duration: String,
            arrival: String,
            arrivalTime: String,
            rating: String,
            ratingCount: String,
            price: String,
            seatsAvailable: Number,
            seatBookedForLowerPlatform: Array,
            seatBookedForUpperPlatform: Array,
        },
        {
            timestamps: true
        }
    );
    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });
    const Bus = mongoose.model("bus", schema);
    return Bus;

}
