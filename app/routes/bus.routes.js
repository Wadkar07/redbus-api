const bus = require("../controller/bus.controller");
const express = require("express");

module.exports =(app)=>{
    let router = express.Router();

    router.post("/", bus.create);

    router.get("/",bus.findAll);

    router.get("/:id",bus.findOne);

    router.put("/:id",bus.update);

    router.delete("/:id",bus.delete);
    
    router.delete("/",bus.deleteAll);

    app.use("/api/buses",router);
}