const cors = require('cors');
const express = require('express');
const app = express();
const db = require('./app/models');
const bus = require('./app/routes/bus.routes.js');
const errorHandler = require('./middleware/errorHandler');

const PORT = process.env.PORT || 8080;

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.json(
        {
            message: "Welcome to redbus api"
        }
    )
})

db.mongoose
.connect(db.url,{
    useNewUrlParser : true,
    useUnifiedTopology : true
})
.then(()=>{
    console.log("connected to db");
})
.catch((err)=>{
    console.log(`${err.message}`);
    process.exit();
})

bus(app);

app.use(errorHandler)

app.listen(PORT,()=>{
    console.log(`Listening at port ${PORT}...`);
})